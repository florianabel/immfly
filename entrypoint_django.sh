#!/bin/sh

if [ "$DATABASE_TYPE" = "psql" ]
then
    echo "Waiting for database..."

    while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# Only in development!!!
python backend/manage.py runserver 0.0.0.0:8000
