#!/bin/sh

if [ "$DATABASE_TYPE" = "psql" ]
then
    echo "Waiting for database..."

    while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

cd backend
python manage.py collectstatic --no-input
gunicorn --bind 0.0.0.0:8000 backend.wsgi:application