# immfly take home exercise



## Overview

The goal of the exercise was to build a Django application to serve a plane's entertainment system.

### Subgoals
- [ ] The system should model a recursive channel structure with content underneath.
- [ ] The system provides endpoints to request channels, subchannels and content objects
- [ ] A management command can be used to calculate average ratings for each channel and export a sorted list as csv
- [ ] Unit tests for the rating algorithm are provided

### Bonus
- [ ] Channels can be added to Group objects
- [ ] Containerization of the project with Docker
- [ ] Usage of Gitlab CI/CD
- [ ] Passing strict type annotation testing with mypy
- [ ] High test coverage in general


## Solution

In order to fulfill the requirements I implemented a system with the following components:

### Components
- [ ] Django Web Application with DRF and a PostgreSQL Database
    - [ ] Application "users" with an empty implementation of AbstractUser for future extension
        - [ ] Basic test cases
    - [ ] Application "entertainment" with:
        - [ ] Models for ContentLanguage, Content, Channel, and Group objects
        - [ ] Serializers to use with DRF and above mentioned models
        - [ ] ReadOnlyModelViewSets for Channel and Content resources, allowing listing and retrieving of singular resources by pk. Also allowing the 
filtering of channels by group parameter.
        - [ ] Added ViewSets to Urls by using a router object
        - [ ] Added all resources to the Admin Panel for easy interaction
        - [ ] Basic test cases
- [ ] (For the moment I omitted a caching solution like Redis or MemCached)
- [ ] Gunicorn Webserver and NGINX as reverse-proxy and to serve static/media files in production
- [ ] Containerization with Docker and Docker Compose, running Django and PSQL
    - [ ] See Dockerfile, docker.compose.yml, docker-compose.prod.yml, ...
- [ ] Start and entrypoint bash scripts for local development and production (usage: see below, section: "Usage")
    - [ ] See entrypoint_django.sh, start.sh, ...
- [ ] Linting with Flake8
- [ ] Type checking with mypy
- [ ] Unit testing with builtin django test framework
- [ ] Set up Gitlab CI/CD for Linting, type checking and unit testing on commit && push


### Usage
- [ ] Start development environment:
    - [ ] Create a copy of 'sample.env' and save as '.env'
    - [ ] Set DJANGO_DEBUG="True"
    - [ ] Adjust parameter as desired
    - [ ] Run development start script './start.sh'

    On first start:
    - [ ] Get container id: Run 'docker ps'
    - [ ] Tunnel into container: Run 'docker exec -it <cotainer_id> bash'
    - [ ] Change directory: Run 'cd backend'
    - [ ] Run migrations: Run 'python manage.py migrate'
    - [ ] Create superuser: Run 'python manage.py createsuperuser'
    - [ ] Access Admin Panel if necesarry on 'localhost/admin'
- [ ] Start production environment:
    - [ ] Create a copy of 'sample.env' and save as '.env'
    - [ ] Set DJANGO_DEBUG="False"
    - [ ] Adjust parameter as desired
    - [ ] Run production start script './start.prod.sh'

    On first start:
    - [ ] Get container id: Run 'docker ps'
    - [ ] Tunnel into container: Run 'docker exec -it <cotainer_id> bash'
    - [ ] Change directory: Run 'cd backend'
    - [ ] Run migrations: Run 'python manage.py migrate'
    - [ ] Create superuser: Run 'python manage.py createsuperuser'
    - [ ] Access Admin Panel if necesarry on 'localhost/admin'
- [ ] Resources (Channels, Content, etc.) can be added through the Admin panel
- [ ] Resources can be requested through their endpoints:
    - [ ] Channels: 
        - [ ] List: /api/v1/channels/
        - [ ] Filtered-List: /api/v1/channels/?group?<title>
        - [ ] Detail: /api/v1/channels/<pk>
    - [ ] Content:
        - [ ] List: /api/v1/content/
        - [ ] Detail: /api/v1/content/<pk>
- [ ] Export average channel ratings: The management command 'python manage.py exportaverageratings' creates a timestamped csv in the folder "exports" next 
to the django application folder
- [ ] Linting: Enter django application folder and run 'flake8 .' (Make sure to exec into the running container or to provide all the dependencies)
- [ ] Type annotation checking: Enter django application folder and run 'mypy .' (Make sure to exec into the running container or to provide all the 
dependencies)
- [ ] Unittesting: Enter django application folder and run 'python manage.py test' (Make sure to exec into the running container or to provide all the 
dependencies)
- [ ] Alternatively: Lint, check type annotations and run tests on commit && push to gitlab

