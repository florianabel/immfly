FROM python:3.10.0

# create directory for the npc user
RUN mkdir -p /home/npc

# create the npc user
RUN addgroup --system npc && adduser --system --group npc

# create the appropriate directories
ENV HOME=/home/npc
ENV NPC_HOME=/home/npc/web
RUN mkdir $NPC_HOME

# set work directory
WORKDIR $NPC_HOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# update packages
RUN apt-get -qq update && \
    apt-get install --yes apache2 apache2-dev  && \
    apt-get install ncat --yes && \
    rm -r /var/lib/apt/lists/*

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt $NPC_HOME
RUN pip install -r requirements.txt

# copy project
COPY . $NPC_HOME

# chown all the files to the npc user
RUN chown -R npc:npc $NPC_HOME

# change to the npc user
USER npc
