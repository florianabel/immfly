from django.test import TestCase

from django.contrib.auth import get_user_model


# Create your tests here.
class UserManagerTests(TestCase):

    def test_create_user(self) -> None:
        User = get_user_model()
        user = User.objects.create_user(
            username='moviemarathon77',
            email='peter.bauer@gmail.com',
            password='random_password_1$3'
        )

        self.assertEqual(user.username, 'moviemarathon77')
        self.assertEqual(user.email, 'peter.bauer@gmail.com')

        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

        with self.assertRaises(TypeError):
            User.objects.create_user()  # type: ignore
        with self.assertRaises(ValueError):
            User.objects.create_user(username='')
        with self.assertRaises(ValueError):
            User.objects.create_user(username='', password='random')

    def test_create_superuser(self) -> None:
        User = get_user_model()
        superuser = User.objects.create_superuser(
            username='immfly_manager_37',
            email='martin.thomas@immfly.com',
            password='random_password_1$3'
        )

        self.assertEqual(superuser.username, 'immfly_manager_37')
        self.assertEqual(superuser.email, 'martin.thomas@immfly.com')

        self.assertTrue(superuser.is_active)
        self.assertTrue(superuser.is_staff)
        self.assertTrue(superuser.is_superuser)

        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                username='intruder',
                password='random_3_4',
                is_superuser=False
            )
