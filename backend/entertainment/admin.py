from django.contrib import admin

from .models import ContentLanguage, Content, Channel, Group

# Register your models here.
admin.site.register(ContentLanguage)
admin.site.register(Content)
admin.site.register(Channel)
admin.site.register(Group)
