from django.urls import path, include
from rest_framework import routers

from .views import ChannelViewSet, ContentViewSet

router = routers.DefaultRouter()
router.register(r'channels', ChannelViewSet)
router.register(r'content', ContentViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls)),
]
