import os
from io import StringIO

from django.test import TestCase
from django.core.files import File
from django.db.utils import IntegrityError
from django.core.management import call_command

from .models import ContentLanguage, Content, Channel


# Create your tests here.
class ContentTestCase(TestCase):
    def setUp(self) -> None:
        path = os.getcwd()

        english = ContentLanguage.objects.create(name='English')
        spanish = ContentLanguage.objects.create(name='Spanish')
        french = ContentLanguage.objects.create(name='French')
        # german = ContentLanguage.objects.create(name='German')
        # portuguese = ContentLanguage.objects.create(name='Portuguese')

        with open(f"{path}/../dummy_data/dummy_thumbnail.png", 'rb') as thumb:
            thumbnail_file = File(thumb, name='thumbnail')
            series_channel = Channel.objects.create(
                title='Series',
                thumbnail=thumbnail_file
            )
            series_channel.languages.add(english)
            series_channel.languages.add(spanish)

        with open(f"{path}/../dummy_data/dummy_thumbnail.png", 'rb') as thumb:
            thumbnail_file = File(thumb, name='thumbnail')
            vikings_channel = Channel.objects.create(
                title='Vikings',
                thumbnail=thumbnail_file
            )
            vikings_channel.languages.add(english)
            vikings_channel.languages.add(spanish)
            vikings_channel.parent_channel = series_channel
            vikings_channel.save()

        with open(f"{path}/../dummy_data/dummy_file.txt", mode='rb') as f:
            with open(
                f"{path}/../dummy_data/dummy_thumbnail.png", 'rb'
            ) as thumb:
                content_file = File(f, name='content_file')
                thumbnail_file = File(thumb, name='thumbnail')
                content_1 = Content.objects.create(
                    content_type=Content.ContentTypes.VIDEO,
                    title='S1E1Vikings',
                    description='First episode of the series Vikings',
                    authors='Peter Parker, Larry Page, Sebastian Borrel',
                    genre='Adventure',
                    rating=3.5,
                    season=1,
                    episode=1,
                    content=content_file,
                    thumbnail=thumbnail_file,
                    parent_channel=vikings_channel
                )
            content_1.audio_languages.add(english)
            content_1.audio_languages.add(spanish)
            content_1.subtitle_languages.add(english)
            content_1.subtitle_languages.add(spanish)
            content_1.subtitle_languages.add(french)

        with open(f"{path}/../dummy_data/dummy_file.txt", mode='rb') as f:
            with open(
                f"{path}/../dummy_data/dummy_thumbnail.png", 'rb'
            ) as thumb:
                content_file = File(f, name='content_file')
                thumbnail_file = File(thumb, name='thumbnail')
                content_2 = Content.objects.create(
                    content_type=Content.ContentTypes.VIDEO,
                    title='S1E2Vikings',
                    description='Second episode of the series Vikings',
                    authors='Peter Parker, Larry Page, Sebastian Borrel',
                    genre='Adventure',
                    rating=4.5,
                    season=1,
                    episode=2,
                    content=content_file,
                    thumbnail=thumbnail_file,
                    parent_channel=vikings_channel
                )
            content_2.audio_languages.add(english)
            content_2.audio_languages.add(spanish)
            content_2.subtitle_languages.add(english)
            content_2.subtitle_languages.add(spanish)
            content_2.subtitle_languages.add(french)

        with open(f"{path}/../dummy_data/dummy_file.txt", mode='rb') as f:
            with open(
                f"{path}/../dummy_data/dummy_thumbnail.png", 'rb'
            ) as thumb:
                content_file = File(f, name='content_file')
                thumbnail_file = File(thumb, name='thumbnail')
                content_3 = Content.objects.create(
                    content_type=Content.ContentTypes.VIDEO,
                    title='S1E3Vikings',
                    description='Third episode of the series Vikings',
                    authors='Peter Parker, Larry Page, Sebastian Borrel',
                    genre='Adventure',
                    rating=4.0,
                    season=1,
                    episode=3,
                    content=content_file,
                    thumbnail=thumbnail_file,
                    parent_channel=vikings_channel
                )
            content_3.audio_languages.add(english)
            content_3.audio_languages.add(spanish)
            content_3.subtitle_languages.add(english)
            content_3.subtitle_languages.add(spanish)
            content_3.subtitle_languages.add(french)

    def test_content(self) -> None:
        vikings_1 = Content.objects.get(title='S1E1Vikings')
        self.assertEqual(
            vikings_1.description, 'First episode of the series Vikings'
        )
        self.assertEqual(vikings_1.audio_languages.count(), 2)
        self.assertEqual(vikings_1.subtitle_languages.count(), 3)
        self.assertEqual(vikings_1.rating, 3.5)
        for line in vikings_1.content:
            self.assertEqual(line, b"dummy file text")

        all_content = Content.objects.all()
        self.assertEqual(all_content.count(), 3)

        series_channel = Channel.objects.get(title='Series')
        self.assertEqual(series_channel.subchannels.count(), 1)

        vikings_channel = Channel.objects.get(title='Vikings')
        self.assertEqual(vikings_channel.content_for_channel.count(), 3)

        # self.assertEqual(vikings_channel.rating, 4.0)
        # self.assertAlmostEqual(vikings_channel.rating, 4.0)

        # self.assertEqual(series_channel.rating, 4.0)

    def test_rating_constraint_upper(self) -> None:
        """
        Check if rating out of range throws IntegrityError
        """
        path = os.getcwd()

        with open(f"{path}/../dummy_data/dummy_file.txt", mode='rb') as f:
            with open(
                f"{path}/../dummy_data/dummy_thumbnail.png", 'rb'
            ) as thumb:
                content_file = File(f, name='content_file')
                thumbnail_file = File(thumb, name='thumbnail')
                with self.assertRaises(IntegrityError):
                    Content.objects.create(
                        content_type=Content.ContentTypes.VIDEO,
                        title='S1E4Vikings',
                        description='Fourth episode of the series Vikings',
                        authors='Peter Parker, Larry Page, Sebastian Borrel',
                        genre='Adventure',
                        rating=11.5,
                        season=1,
                        episode=1,
                        content=content_file,
                        thumbnail=thumbnail_file,
                    )

    def test_rating_constraint_lower(self) -> None:
        """
        Check if rating out of range throws IntegrityError
        """
        path = os.getcwd()

        with open(f"{path}/../dummy_data/dummy_file.txt", mode='rb') as f:
            with open(
                f"{path}/../dummy_data/dummy_thumbnail.png", 'rb'
            ) as thumb:
                content_file = File(f, name='content_file')
                thumbnail_file = File(thumb, name='thumbnail')

                with self.assertRaises(IntegrityError):
                    Content.objects.create(
                        content_type=Content.ContentTypes.VIDEO,
                        title='S1E5Vikings',
                        description='Fifth episode of the series Vikings',
                        authors='Peter Parker, Larry Page, Sebastian Borrel',
                        genre='Adventure',
                        rating=-2.5,
                        season=1,
                        episode=1,
                        content=content_file,
                        thumbnail=thumbnail_file,
                    )

    def test_calculate_channel_ratings(self) -> None:
        out = StringIO()
        call_command("exportaverageratings", stdout=out)

        self.assertIn("'Vikings': Decimal('4')", out.getvalue())
        self.assertIn("'Series': Decimal('4')", out.getvalue())
