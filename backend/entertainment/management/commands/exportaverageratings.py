import os
import csv
from typing import Dict, Any, Tuple
from decimal import Decimal

from statistics import mean
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Avg

from entertainment.models import Content, Channel


class Command(BaseCommand):
    help = 'Exports the average rating of each channel as csv'

    def handle(self, *args: Tuple[Any], **kwargs: Dict[str, Any]) -> None:
        content_objects = Content.objects.all()
        average_ratings_for_channels: Dict[str, Decimal] = {}

        parent_channels = list(set([
            content.parent_channel.title
            for content
            in content_objects
            if content.parent_channel is not None
        ]))
        while len(parent_channels) > 0:
            qs = Channel.objects.filter(title__in=parent_channels)
            for channel in qs:
                key = channel.title
                content = channel.content_for_channel
                subchannels = channel.subchannels

                if content.count() > 0 and subchannels.count() > 0:
                    raise CommandError(
                        f"Channel {key} has content and subchannel"
                    )

                if content.count() > 0:
                    average = content.aggregate(rating=Avg('rating'))['rating']
                elif subchannels.count() > 0:
                    channel_ratings = [
                        average_ratings_for_channels.get(channel_name)
                        for channel_name
                        in channel.subchannels.values_list('title', flat=True)
                        if average_ratings_for_channels.get(channel_name)
                        is not None
                    ]
                    if len(channel_ratings) > 0:
                        average = mean(channel_ratings)  # type: ignore
                    else:
                        average = None
                average_ratings_for_channels[key] = (
                    average.normalize() if average is not None else average
                )

                parent_channels.clear()
                if channel.parent_channel:
                    parent_channels.append(channel.parent_channel.title)

            parent_channels = list(set(parent_channels))
        now_utc = datetime.utcnow()
        valid_ratings = dict(filter(
            lambda item: item[1] is not None,
            average_ratings_for_channels.items()
        ))
        none_ratings = dict(filter(
            lambda item: item[1] is None,
            average_ratings_for_channels.items()
        ))

        os.makedirs("../exports", exist_ok=True)
        with open(f"../exports/ratings_{now_utc}.csv", 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['channel', 'average_rating'])
            for tup in sorted(
                valid_ratings.items(),
                key=lambda x: x[1],
                reverse=True
            ):
                writer.writerow(tup)
            for key, value in none_ratings.items():
                writer.writerow([key, value])
        self.stdout.write(str(average_ratings_for_channels))
