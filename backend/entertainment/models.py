from django.db import models
from django.db.models import Q


# Create your models here.
class ContentLanguage(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.name


class Channel(models.Model):
    title = models.CharField(max_length=50)
    languages = models.ManyToManyField(
        ContentLanguage, related_name='channels')
    thumbnail = models.ImageField(upload_to='thumbnails/')
    parent_channel = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='subchannels'
    )

    def __str__(self) -> str:
        return self.title


class Content(models.Model):
    class ContentTypes(models.IntegerChoices):
        VIDEO = 1
        AUDIO = 2
        PDF = 3
        TEXT = 4

    content_type = models.IntegerField(choices=ContentTypes.choices)
    title = models.CharField(max_length=50)
    description = models.TextField()
    authors = models.CharField(max_length=200)
    genre = models.CharField(max_length=20)
    audio_languages = models.ManyToManyField(
        ContentLanguage,
        related_name='content_for_audio'
    )
    subtitle_languages = models.ManyToManyField(
        ContentLanguage, related_name='content_for_subtitle')
    rating = models.DecimalField(
        max_digits=3,
        decimal_places=1,
        null=True,
        blank=True
    )
    content = models.FileField(upload_to='content/')
    thumbnail = models.ImageField(upload_to='thumbnails/')
    season = models.IntegerField(null=True, blank=True)
    episode = models.IntegerField(null=True, blank=True)
    parent_channel = models.ForeignKey(
        Channel,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='content_for_channel'
    )

    def __str__(self) -> str:
        return self.title

    class Meta:
        constraints = [
            models.CheckConstraint(
                name='check_rating_range',
                check=(Q(rating__gte=0.0) & Q(rating__lte=10.0))
            ),
        ]


class Group(models.Model):
    title = models.CharField(max_length=50)
    channels = models.ManyToManyField(
        Channel,
        related_name='groups_for_channel'
    )

    def __str__(self) -> str:
        return self.title
