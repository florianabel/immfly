from django.db.models import Q
from django.http import HttpRequest

from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response

from .models import Channel, Content, Group
from .serializers import ChannelSerializer, ContentSerializer


# Create your views here.
class ChannelViewSet(ReadOnlyModelViewSet):  # type: ignore
    serializer_class = ChannelSerializer
    queryset = Channel.objects.all()

    def list(self, request: HttpRequest) -> Response:
        queryset = Channel.objects.all()
        group = self.request.query_params.get('group')

        if group is not None:
            # Filter by group
            try:
                group_obj = Group.objects.get(title=group)
                print(group_obj)
                print(queryset.count())
                queryset = queryset.filter(groups_for_channel=group_obj)
                print(queryset.count())
            except Group.DoesNotExist:
                pass
        else:
            queryset = queryset.filter(parent_channel__isnull=True)

        # Filter out channels without content and without subchannel
        with_parent_channels = Channel.objects \
            .filter(parent_channel__in=queryset) \
            .distinct() \
            .values_list('parent_channel', flat=True)
        content_with_channel = Content.objects \
            .filter(parent_channel__in=queryset) \
            .distinct() \
            .values_list('parent_channel', flat=True)
        queryset = queryset.filter(
            Q(pk__in=with_parent_channels)
            | Q(pk__in=content_with_channel)
        )

        # Filter out channels that have content and subchannel
        double_use_channels = list(
            set(with_parent_channels).intersection(content_with_channel)
        )
        queryset = queryset.exclude(id__in=double_use_channels)

        serializer = ChannelSerializer(queryset, many=True)
        return Response(serializer.data)


class ContentViewSet(ReadOnlyModelViewSet):  # type: ignore
    serializer_class = ContentSerializer
    queryset = Content.objects.all()
