from typing import Optional, Any

from django.db.models import Q, QuerySet
from rest_framework import serializers

from .models import Channel, Content, Group


class ContentSerializer(serializers.ModelSerializer):  # type: ignore
    class Meta:
        model = Content
        fields = [
            'id',
            'content_type',
            'title',
            'description',
            'authors',
            'genre',
            'audio_languages',
            'subtitle_languages',
            'rating',
            'content',
            'thumbnail',
            'season',
            'episode',
            'parent_channel'
        ]


class GroupSerializer(serializers.ModelSerializer):  # type: ignore
    class Meta:
        model = Group
        fields = ['id', 'title', 'channels']


class ChannelSerializer(serializers.ModelSerializer):  # type: ignore
    subchannels = serializers.SerializerMethodField()
    groups = serializers.SerializerMethodField()
    content_for_channel = ContentSerializer(many=True)

    class Meta:
        model = Channel
        fields = [
            'id',
            'title',
            'languages',
            'thumbnail',
            'subchannels',
            'content_for_channel',
            'parent_channel',
            'groups'
        ]

    def get_subchannels(self, obj: Any) -> Optional[Any]:
        channels = obj.subchannels.all()

        # Filter out channels without content and without subchannel
        with_parent_channels = Channel.objects \
            .filter(parent_channel__in=channels) \
            .distinct() \
            .values_list('parent_channel', flat=True)
        content_with_channel = Content.objects \
            .filter(parent_channel__in=channels) \
            .distinct() \
            .values_list('parent_channel', flat=True)
        channels = channels.filter(
            Q(pk__in=with_parent_channels)
            | Q(pk__in=content_with_channel)
        )

        # Filter out channels that have content and subchannel
        double_use_channels = list(
            set(with_parent_channels).intersection(content_with_channel)
        )
        channels = channels.exclude(id__in=double_use_channels)

        if channels.count() > 0:
            serializer = ChannelSerializer(channels, many=True)
            return serializer.data
        else:
            return None

    def fetch_subchannels(self, channel: Channel) -> QuerySet[Channel]:
        subchannels = channel.subchannels.all()
        for subchannel in subchannels:
            child_subchannels = self.fetch_subchannels(subchannel)
            subchannels = subchannels | child_subchannels
        return subchannels

    def get_groups(self, obj: Channel) -> Any:
        groups = obj.groups_for_channel.all()
        subchannels = self.fetch_subchannels(obj)

        if subchannels:
            groups = groups | Group.objects \
                .filter(channels__in=subchannels).all()

        groups = groups.distinct()
        serializer = GroupSerializer(groups, many=True)
        return serializer.data
